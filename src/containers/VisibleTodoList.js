import { connect } from 'react-redux';

import { toggleTodo, setEditableTodo, unSetEditableTodo } from '../actionCreators';
import { updateTodo, deleteTodo } from '../actions/todos';
import TodoList from '../components/TodoList'

const getVisibleTodos = (todos, filter) => {

  switch (filter) {
    case 'SHOW_COMPLETED':
      return todos.filter(t => t.completed)
    case 'SHOW_ACTIVE':
      return todos.filter(t => !t.completed)
    case 'SHOW_ALL':
    default:
      return todos
  }
}

const mapStateToProps = state => {
  return {
    todos: getVisibleTodos(state.todos.items, state.visibilityFilter),
    editableTodo: state.editableTodo
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTodoClick(id, completed) {
      updateTodo(dispatch, id, { completed });
      dispatch(toggleTodo(id));
    },
    onDelBtnClick(id) {
      deleteTodo(dispatch, id)
    },
    onEditBtnClick(id) {
      dispatch(setEditableTodo(id));
    },
    onUpdate(id, text) {
      updateTodo(dispatch, id, {text});
      dispatch(unSetEditableTodo());
    }
  }
}

const VisibleTodoList = connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList)

export default VisibleTodoList
