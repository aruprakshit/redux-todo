import React from "react";
import { connect } from "react-redux";
import faker from 'faker';

import { createTodo } from "../actions/todos";

let AddTodo = ({ dispatch }) => {
  let _input;
  const addNewTodo = () => {
    const text = _input.value.trim();

    if (!text) {
      return;
    }

    createTodo(dispatch, { text, completed: false, author: faker.name.findName()})
    _input.value = "";
  };

  return (
    <div id="myDIV" className="header">
      <h2 style={{ margin: "5px" }}>My To Do List</h2>
      <input
        type="text"
        id="myInput"
        placeholder="Title..."
        ref={node => (_input = node)}
      />
      <span onClick={addNewTodo} className="addBtn">
        Add
      </span>
    </div>
  );
};

export default connect()(AddTodo);
