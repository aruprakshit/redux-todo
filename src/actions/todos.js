import { database } from "../firebase";

import {
  addTodo,
  fetchTodosSuccess,
  fetchTodosRequest,
  updateTodosRequest,
  updateTodosSuccess,
  deleteTodosRequest,
  deleteTodosSuccess
} from "../actionCreators";

export function createTodo(dispatch, payload) {
  const newTodoRef = database
    .ref()
    .child("todos")
    .push();
  newTodoRef
    .set(payload)
    .then(() => dispatch(addTodo({ id: newTodoRef.key, ...payload })));
}

export function fetchTodos(dispatch) {
  dispatch(fetchTodosRequest());

  database
    .ref()
    .once("value")
    .then((snapshot) => {
      const payload = [];
      const { todos } = snapshot.val();

      for (const id in todos) {
        payload.push({ id, ...todos[id] });
      }

      dispatch(fetchTodosSuccess(payload));
    });
}

export function updateTodo(dispatch, id, payload) {
  dispatch(updateTodosRequest());

  database
    .ref('todos')
    .child(id)
    .update(payload)
    .then(() => dispatch(updateTodosSuccess({ id, ...payload })))
}

export function deleteTodo(dispatch, id) {
  dispatch(deleteTodosRequest());
  database
    .ref('todos')
    .child(id)
    .remove()
    .then(() => dispatch(deleteTodosSuccess(id)))
}
