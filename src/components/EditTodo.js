import React from "react";
import PropTypes from "prop-types";

const EditTodo = ({ text, onUpdate }) => {
  let _form;

  const getFormValues = (e) => {
    e.preventDefault();
    onUpdate(_form.elements[0].value);
  }

  return (
    <form onSubmit={getFormValues} ref={f => _form = f}>
      <input type="text" defaultValue={text} className="input-text"/>
      <input type="submit" value="Save" className='save-edit-btn'/>
    </form>
  );
};

EditTodo.propTypes = {
  onUpdate: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired
};

export default EditTodo;
