import React from "react";
import PropTypes from "prop-types";

import EditTodo from "./EditTodo";

const Todo = ({
  onClick,
  completed,
  text,
  deleTodoItem,
  isEditable,
  onEditBtnClick,
  onUpdate
}) => {
  if (isEditable)
    return <li className="todo-item d-flex"><EditTodo text={text} isEditable={isEditable} onUpdate={onUpdate} /></li>;

  const onButtonClick = e => {
    if (e.target.dataset.title === "remove") {
      e.stopPropagation();
      deleTodoItem();
    } else if (e.target.dataset.title === "edit") {
      e.stopPropagation();
      onEditBtnClick();
    }
  };

  return (
    <li
      onClick={() => onClick(!completed)}
      className="todo-item d-flex"
      style={{
        textDecoration: completed ? "line-through" : "none"
      }}
    >
      <span>{text}</span>
      <div className='actions'>
        <span className="edit-o" onClick={onButtonClick} data-title="remove">&#x2716;</span>
        <span className="thrash-o" onClick={onButtonClick} data-title="edit">&#9998;</span>
      </div>
    </li>
  );
};

Todo.propTypes = {
  onClick: PropTypes.func.isRequired,
  deleTodoItem: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
};

export default Todo;
