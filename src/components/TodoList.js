import React from "react";
import PropTypes from "prop-types";
import Todo from "./Todo";

const TodoList = ({
  todos,
  onTodoClick,
  onDelBtnClick,
  editableTodo,
  onEditBtnClick,
  onUpdate
}) => (
  <ul id="myUL">
    {todos.map(todo => (
      <Todo
        isEditable={editableTodo === todo.id}
        key={todo.id}
        {...todo}
        onClick={(completed) => onTodoClick(todo.id, completed)}
        deleTodoItem={() => onDelBtnClick(todo.id)}
        onEditBtnClick={() => onEditBtnClick(todo.id)}
        onUpdate={(text) => onUpdate(todo.id, text)}
      />
    ))}
  </ul>
);

TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onTodoClick: PropTypes.func.isRequired
};

export default TodoList;
