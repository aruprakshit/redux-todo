import React, {PureComponent, Fragment} from 'react';
import { connect } from 'react-redux';

import Footer from './Footer';
import AddTodo from '../containers/AddTodo';
import VisibleTodoList from '../containers/VisibleTodoList';
import { fetchTodos } from '../actions/todos';

class App extends PureComponent {
  componentDidMount() {
    fetchTodos(this.props.dispatch);
  }

  render() {
    return (
      <Fragment>
        <AddTodo />
        <VisibleTodoList />
        <Footer />
      </Fragment>
    )
  }
}

export default connect()(App);
