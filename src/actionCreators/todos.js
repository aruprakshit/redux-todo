export const fetchTodosRequest = () => (
  {
    type: 'FETCH_TODOS_REQUEST'
  }
);

export const fetchTodosSuccess = (payload) => (
  {
    type: 'FETCH_TODOS_SUCCESS',
    payload
  }
);

export const fetchTodosFailure = (payload) => (
  {
    type: 'FETCH_TODOS_FAILURE',
    payload
  }
);

export const updateTodosRequest = () => (
  {
    type: 'UPDATE_TODOS_REQUEST'
  }
);

export const updateTodosSuccess = (payload) => (
  {
    type: 'UPDATE_TODOS_SUCCESS',
    payload
  }
);

export const updateTodosFailure = (payload) => (
  {
    type: 'UPDATE_TODOS_FAILURE',
    payload
  }
);

export const deleteTodosRequest = () => (
  {
    type: 'DELETE_TODOS_REQUEST'
  }
);

export const deleteTodosSuccess = (payload) => (
  {
    type: 'DELETE_TODOS_SUCCESS',
    payload
  }
);

export const deleteTodosFailure = (payload) => (
  {
    type: 'DELETE_TODOS_FAILURE',
    payload
  }
);
