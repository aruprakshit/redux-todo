export {
  fetchTodosRequest,
  fetchTodosSuccess,
  fetchTodosFailure,
  updateTodosRequest,
  updateTodosSuccess,
  updateTodosFailure,
  deleteTodosRequest,
  deleteTodosSuccess,
  deleteTodosFailure
} from "./todos";

export const addTodo = payload => {
  return {
    type: "ADD_TODO",
    ...payload
  };
};

export const showNotification = text => ({
  type: "SHOW_NOTIFICATION",
  text
});

export const hideNotification = () => ({
  type: "HIDE_NOTIFICATION"
});

export const setEditableTodo = id => ({
  type: "EDIT_TODO",
  editableTodo: id
});

export const unSetEditableTodo = id => ({
  type: "UNSET_EDIT_TODO",
  editableTodo: null
});

export const setVisibilityFilter = filter => {
  return {
    type: "SET_VISIBILITY_FILTER",
    filter
  };
};

export const toggleTodo = id => {
  return {
    type: "TOGGLE_TODO",
    id
  };
};
