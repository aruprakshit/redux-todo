import { combineReducers } from "redux";

const isFetching = (state = false, action) => {
  switch (action.type) {
    case "FETCH_TODOS_REQUEST":
      return true;
    case "FETCH_TODOS_SUCCESS":
    case "FETCH_TODOS_FAILURE":
      return false;
    default:
      return state;
  }
};

const isUpdating = (state = false , action) => {
  switch (action.type) {
    case "UPDATE_TODOS_REQUEST":
      return true;
    case "UPDATE_TODOS_SUCCESS":
    case "UPDATE_TODOS_FAILURE":
      return false;
    default:
      return state;
  }
};

const isDeleting = (state = false, action) => {
  switch (action.type) {
    case "DELETE_TODOS_REQUEST":
      return true;
    case "DELETE_TODOS_SUCCESS":
    case "DELETE_TODOS_FAILURE":
      return false;
    default:
      return state;
  }
};

const items = (state = [], action) => {
  switch (action.type) {
    case "ADD_TODO":
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false
        }
      ];
    case "UPDATE_TODOS_SUCCESS":
      return state.map(
        todo => (todo.id === action.payload.id ? { ...todo, ...action.payload } : todo)
      );
    case "TOGGLE_TODO":
      return state.map(
        todo =>
          todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );
    case "FETCH_TODOS_SUCCESS":
      return [...state, ...action.payload];
    case 'DELETE_TODOS_SUCCESS':
      return state.filter(item => item.id !== action.payload);
    default:
      return state;
  }
};

const todos = combineReducers({
  isFetching,
  isUpdating,
  isDeleting,
  items
});

export default todos;
