import { combineReducers } from 'redux';
import todos from './todos';
import visibilityFilter from './visibilityFilter';
import editableTodo from './editableTodo';

const todoApp = combineReducers({
  todos,
  visibilityFilter,
  editableTodo
})

export default todoApp;
