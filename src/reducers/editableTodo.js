const editableTodo = (state = null, action) => {
  switch (action.type) {
    case 'EDIT_TODO':
      return action.editableTodo;
    case 'UNSET_EDIT_TODO':
      return action.editableTodo;
    default:
      return state
  }
}

export default editableTodo;
